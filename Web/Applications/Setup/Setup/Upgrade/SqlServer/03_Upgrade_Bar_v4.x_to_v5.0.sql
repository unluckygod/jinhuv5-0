begin tran
-----贴吧板块升级
DELETE FROM [dbo].[tn_Sections]
SET IDENTITY_INSERT [dbo].[tn_Sections] ON
INSERT [dbo].[tn_Sections] (
       [SectionId]
      ,[TenantTypeId]
      ,[OwnerId]
      ,[UserId]
      ,[Name]
      ,[Description]
      ,[FeaturedImageAttachmentId]
      ,[IsEnabled]
      ,[ThreadCategorySettings]
      ,[DisplayOrder]
      ,[DateCreated]
      ,[PropertyNames]
      ,[PropertyValues])
SELECT 
       [SectionId]
      ,'100003' as [TenantTypeId]
      ,[OwnerId]
      ,[UserId]
      ,[Name]
      ,[Description]
      ,0 as [FeaturedImageAttachmentId]
      ,[IsEnabled]
      ,[ThreadCategoryStatus] as [ThreadCategorySettings]
      ,[DisplayOrder]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[spb_BarSections]
SET IDENTITY_INSERT [dbo].[tn_Sections] OFF
-----贴子升级
DELETE FROM [dbo].[tn_Threads]
SET IDENTITY_INSERT [dbo].[tn_Threads] ON
INSERT [dbo].[tn_Threads] (
       [ThreadId]
      ,[SectionId]
      ,[TenantTypeId]
      ,[OwnerId]
      ,[UserId]
      ,[Author]
      ,[Subject]
      ,[Body]
      ,[IsLocked]
      ,[IsSticky]
      ,[ApprovalStatus]
      ,[IP]
      ,[DateCreated]
      ,[LastModified]
      ,[PropertyNames]
      ,[PropertyValues])
SELECT  [ThreadId]
      ,[SectionId]
      ,'100002' as [TenantTypeId]
      ,[OwnerId]
      ,[UserId]
      ,[Author]
      ,[Subject]
      ,[Body]
      ,[IsLocked]
	   ,[IsSticky]
      ,[AuditStatus] as [ApprovalStatus]
      ,[IP]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[LastModified]
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[spb_BarThreads]

SET IDENTITY_INSERT [dbo].[tn_Threads] OFF

 INSERT [dbo].[tn_OperationLogs] ( [TenantTypeId], [OperationType], [OperationObjectId], [OperationObjectName], [Description], [OperationUserRole], [OperationUserId], [Operator], [OperatorIP], [AccessUrl], [DateCreated]) 
SELECT 
	   CASE  [Source] 
	   WHEN  '贴吧' THEN '100003' 
	   WHEN  '贴子' THEN '100002'
	   WHEN  '资讯' THEN '100011'
	   WHEN  '微博' THEN '100011'
	   WHEN  '评论' THEN '000031'
	   ELSE [Source] END as [TenantTypeId]
      ,[OperationType]
      ,[OperationObjectId]
	  ,[OperationObjectName]
      ,[Description]
	  ,' ' as [OperationUserRole]
      ,[OperatorUserId]
      ,[Operator]
      ,[OperatorIP]
      ,[AccessUrl]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
  FROM [tn.jinhudemoNew].[dbo].[tn_OperationLogs]
  where  [Source] = '帖吧' or [Source] = '帖子'

    -----附件升级
 SET IDENTITY_INSERT [dbo].[tn_Attachments] ON
INSERT [dbo].[tn_Attachments] ([AttachmentId], [AssociateId], [OwnerId], [TenantTypeId], [UserId], [UserDisplayName], [FileName], [FriendlyFileName], [MediaType], [ContentType], [FileLength], [Price], [IP], [ConvertStatus], [DateCreated], [Discription], [IsShowInAttachmentList], [PropertyNames], [PropertyValues], [DisplayOrder]) 
SELECT [AttachmentId]
      ,[AssociateId]
      ,[OwnerId]
      ,
	   CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100001' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[UserId]
      ,[UserDisplayName]
      ,[FileName]
      ,[FriendlyFileName]
      ,[MediaType]
      ,[ContentType]
      ,[FileLength]
      ,[Price]
      ,[IP]
	  ,0 as [ConvertStatus]
      , [DateCreated]
	  ,'' as [Discription]
	  ,0 as [IsShowInAttachmentList]
      ,[PropertyNames]
      ,[PropertyValues]
	  ,[AttachmentId] as [DisplayOrder]
  FROM [tn.jinhudemoNew].[dbo].[tn_Attachments] where [TenantTypeId] in ('101201','101202') 
  
  SET IDENTITY_INSERT [dbo].[tn_Attachments] OFF
     -----收藏升级
  insert [dbo].[tn_Favorites]( 
      [TenantTypeId]
      ,[UserId]
      ,[ObjectId])
SELECT   
       CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002'
	   WHEN  '101201' THEN '100003'  
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[UserId]
      ,[ObjectId]
  FROM [tn.jinhudemoNew].[dbo].[tn_Favorites] where [TenantTypeId] in('101201','101202') 

   -----评论升级
SET IDENTITY_INSERT [dbo].[tn_Comments] ON
INSERT [dbo].[tn_Comments] ([Id]
      ,[ParentIds]
      ,[ParentId]
      ,[CommentedObjectId]
      ,[TenantTypeId]
      ,[CommentType]
      ,[ChildrenCount]
      ,[OwnerId]
      ,[UserId]
      ,[Author]
      ,[Subject]
      ,[Body]
      ,[IsAnonymous]
      ,[IsPrivate]
      ,[ApprovalStatus]
      ,[IP]
      ,[DateCreated]
      )
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT 
       postid+1000000 as Id,
       CASE  
	   WHEN  [ParentId]>0 THEN '0,'+(CAST([ParentId]+1000000 as nvarchar(125)) +',')
	   ELSE '0,' END as [ParentIds],
	     CASE  
	   WHEN  [ParentId]>0 THEN [ParentId]+1000000
	   ELSE 0 END as [ParentId],
	    ThreadId as [CommentedObjectId]
      ,'100002' as [TenantTypeId]
       ,'' as [CommentType]
       ,[ChildPostCount] as [ChildrenCount]
      ,[OwnerId]
      ,[UserId]
      ,[Author]
      ,[Subject]
      ,[Body]
      ,0 as [IsAnonymous]
      ,0 as [IsPrivate]
      ,[AuditStatus] as [ApprovalStatus]
      ,[IP]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
  FROM [tn.jinhudemoNew].[dbo].[spb_BarPosts]
   SET IDENTITY_INSERT [dbo].[tn_Comments] OFF
  -----点赞升级
SET IDENTITY_INSERT [dbo].[tn_Attitudes] ON
INSERT [dbo].[tn_Attitudes] ([Id]
      , [TenantTypeId]
      ,[ObjectId]
      ,[SupportCount])
SELECT [Id]
       , CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[ObjectId]
      ,[SupportCount]
  FROM [tn.jinhudemoNew].[dbo].[tn_Attitudes] where [TenantTypeId] in('101201','101202') 
SET IDENTITY_INSERT [dbo].[tn_Attitudes] OFF
-----点赞记录升级
SET IDENTITY_INSERT [dbo].[tn_AttitudeRecords] ON
INSERT [dbo].[tn_AttitudeRecords] ([Id]
      ,[TenantTypeId]
      ,[ObjectId]
      ,[UserId])
SELECT  [Id]
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[ObjectId]
      ,[UserId]
  FROM [tn.jinhudemoNew].[dbo].[tn_AttitudeRecords] where [TenantTypeId] in('101201','101202') 
SET IDENTITY_INSERT [dbo].[tn_AttitudeRecords] OFF



-----类别升级
SET IDENTITY_INSERT [dbo].[tn_Categories] ON
INSERT [dbo].[tn_Categories] ([CategoryId]
      ,[ParentId]
      ,[OwnerId]
      ,[TenantTypeId]
      ,[CategoryName]
      ,[Description]
      ,[DisplayOrder]
      ,[Depth]
      ,[ChildCount]
      ,[ItemCount]
      ,[ImageAttachmentId]
      ,[LastModified]
      ,[DateCreated]
      ,[PropertyNames]
      ,[PropertyValues])
SELECT [CategoryId]
      ,[ParentId]
      ,[OwnerId]
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[CategoryName]
      ,[Description]
      ,[DisplayOrder]
      ,[Depth]
      ,[ChildCount]
      ,[ItemCount]
      ,0 as [ImageAttachmentId]
      ,[LastModified]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[tn_Categories] where [TenantTypeId] in('101201','101202') 
SET IDENTITY_INSERT [dbo].[tn_Categories] OFF

-----类别和内容关联升级
SET IDENTITY_INSERT [dbo].[tn_ItemsInCategories] ON
INSERT [dbo].[tn_ItemsInCategories] ([Id]
      ,[CategoryId]
      ,[ItemId])
SELECT  [Id]
      ,[tn_ItemsInCategories].[CategoryId]
      ,[ItemId]
  FROM [tn.jinhudemoNew].[dbo].[tn_ItemsInCategories] left join [tn.jinhudemoNew].[dbo].[tn_Categories]  on  [tn.jinhudemoNew].[dbo].[tn_Categories].[CategoryId]  =[tn_ItemsInCategories].[CategoryId] where  [tn.jinhudemoNew].[dbo].[tn_Categories].TenantTypeId in('101201','101202')
SET IDENTITY_INSERT [dbo].[tn_ItemsInCategories] OFF


-----标签升级
SET IDENTITY_INSERT [dbo].[tn_Tags] ON
INSERT [dbo].[tn_Tags] ([TagId]
      ,[TenantTypeId]
      ,[TagName]
      ,[Description]
      ,[ImageAttachmentId]
      ,[IsFeatured]
      ,[ItemCount]
      ,[DateCreated]
      ,[PropertyNames]
      ,[PropertyValues])
SELECT [TagId]
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[TagName]
      ,[Description]
      ,0 as  [ImageAttachmentId]
      ,[IsFeatured]
      ,[ItemCount]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[tn_Tags] where [TenantTypeId] in('101201','101202') 
SET IDENTITY_INSERT [dbo].[tn_Tags] OFF

-----标签关联项升级
SET IDENTITY_INSERT [dbo].[tn_ItemsInTags] ON
INSERT [dbo].[tn_ItemsInTags] ([Id]
      ,[TagName]
      ,[ItemId]
      ,[TenantTypeId])
SELECT  [Id]
      ,[TagName]
      ,[ItemId]
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
  FROM [tn.jinhudemoNew].[dbo].[tn_ItemsInTags] where [TenantTypeId] in('101201','101202') 
SET IDENTITY_INSERT [dbo].[tn_ItemsInTags] OFF


-----栏目管理员升级
DELETE FROM [dbo].[tn_CategoryManagers]
SET IDENTITY_INSERT [dbo].[tn_CategoryManagers] ON
INSERT [dbo].[tn_CategoryManagers] ([Id]
      ,[CategoryId]
      ,[TenantTypeId]
      ,[ReferenceCategoryId]
      ,[UserId])
SELECT [Id]
      ,[SectionId] as [CategoryId]
	  ,'100001' as [TenantTypeId]
	  ,0 as [ReferenceCategoryId]
      ,[UserId]
  FROM [tn.jinhudemoNew].[dbo].[spb_BarSectionManagers]
SET IDENTITY_INSERT [dbo].[tn_CategoryManagers] OFF



-----推荐内容升级
DELETE FROM [dbo].[tn_SpecialContentItems]
-----贴子精华升级
INSERT [dbo].[tn_SpecialContentItems] (
       [TenantTypeId]
      ,[TypeId]
      ,[RegionId]
      ,[ItemId]
      ,[ItemName]
      ,[FeaturedImageAttachmentId]
      ,[Recommender]
      ,[RecommenderUserId]
      ,[DateCreated]
      ,[ExpiredDate]
      ,[DisplayOrder]
  )
SELECT  
      '100002' as [TenantTypeId]
      ,11 as [TypeId]
      ,0 as [RegionId]
      ,[ThreadId] as  [ItemId]
	  ,[Subject] as [ItemName]
      , 0 as [FeaturedImageAttachmentId]
      ,' ' as [Recommender]
	  ,0 as [RecommenderUserId]
      ,  dateadd(hour,8,[DateCreated]) as [DateCreated] 
	  , dateadd(y,20,getdate()) as [ExpiredDate]
	  , [ThreadId] as  [DisplayOrder]
  FROM [tn.jinhudemoNew].[dbo].[spb_BarThreads] where IsEssential=1

  -----贴吧热词更新
delete from [tn_SearchWords]  where SearchTypeCode = 'Thread'

  SET IDENTITY_INSERT [dbo].[tn_SearchWords] ON
INSERT [dbo].[tn_SearchWords] (
id,
       [Word]
      ,[SearchTypeCode]
      ,[IsAddedByAdministrator]
      ,[DateCreated]
      ,[LastModified])
	  SELECT  
	  Id,
      [Term] as [Word]

      ,'Thread' as [SearchTypeCode]
      ,[IsAddedByAdministrator]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[LastModified]
  FROM [tn.jinhudemoNew].[dbo].[tn_SearchedTerms] where SearchTypeCode = 'BarSearcher'
  SET IDENTITY_INSERT [dbo].[tn_SearchWords] OFF
   commit tran