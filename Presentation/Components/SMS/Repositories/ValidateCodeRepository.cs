﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.Repositories;

namespace Tunynet.Common
{
    /// <summary>
    /// 手机验证码仓储
    /// </summary>
    public class ValidateCodeRepository : Repository<ValidateCode>
    {
    }
}