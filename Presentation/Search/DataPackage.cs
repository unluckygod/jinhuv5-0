﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------
namespace Tunynet.Search
{
    /// <summary>
    /// 数据包
    /// </summary>
    public class DataPackage
    {
        /// <summary>
        /// 类型
        /// </summary>
        private DataType type;

        /// <summary>
        /// 类型
        /// </summary>
        public DataType Type
        {
            get { return type; }
        }

        /// <summary>
        /// 数据
        /// </summary>
        private object data;

        /// <summary>
        /// 数据
        /// </summary>
        public object Data
        {
            get { return data; }
        }

        /// <summary>
        /// 分页
        /// </summary>
        private object page;

        /// <summary>
        /// 分页
        /// </summary>
        public object Page
        {
            get { return page; }
        }

        /// <summary>
        /// 备注
        /// </summary>
        private string description;

        /// <summary>
        /// 备注
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        /// <summary>
        /// 全文检索数据包
        /// </summary>
        /// <param name="_data"></param>
        /// <param name="_page"></param>
        /// <param name="_desc"></param>
        public DataPackage(object _data = null, object _page = null, string _desc = null)
        {
            this.data = _data;
            this.page = _page;
            this.description = _desc;
        }
    }

    /// <summary>
    /// 数据类型的枚举
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// 否决(登录身份问题)
        /// </summary>
        Deny = 0,

        /// <summary>
        /// 正常进行(逻辑正常)
        /// </summary>
        Normal = 1,

        /// <summary>
        /// 异常进行(逻辑错误)
        /// </summary>
        Uncommon = 2
    }

  
}