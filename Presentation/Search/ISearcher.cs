﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

namespace Tunynet.Search
{
    /// <summary>
    /// Searcher接口
    /// </summary>
    public interface ISearcher
    {
        /// <summary>
        /// 搜索类型代码
        /// </summary>
        string Code { get; }

        /// <summary>
        /// 名称
        /// </summary>
        string Name { get; }

        /// <summary>
        /// 显示顺序
        /// </summary>
        int DisplayOrder { get; }

        /// <summary>
        /// Lucene索引路径（完整物理路径，支持unc）
        /// </summary>
        string IndexPath { get; }

        /// <summary>
        /// 重建索引
        /// </summary>
        void RebuildIndex();

        /// <summary>
        /// 关联的搜索引擎实例
        /// </summary>
        ISearchEngine SearchEngine { get; }
    }
}