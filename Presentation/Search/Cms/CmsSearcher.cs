﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Lucene.Net.Documents;
using Lucene.Net.Search;
using System.Collections.Generic;
using System.Linq;
using Tunynet;
using Tunynet.CMS;
using Tunynet.Common;
using Tunynet.Search;

namespace Tunynet.CMS
{
    /// <summary>
    /// 资讯搜索器
    /// </summary>
    public class CmsSearcher : ISearcher
    {
        private ContentItemService contentItemService = DIContainer.Resolve<ContentItemService>();
        private IKvStore kvStore = DIContainer.Resolve<IKvStore>();
        private ISearchEngine searchEngine;

        /// <summary>
        /// code
        /// </summary>
        public static string CODE = "CmsSearcher";

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="indexPath"></param>
        /// <param name="displayOrder"></param>
        public CmsSearcher(string name, string indexPath, int displayOrder)
        {
            this.Name = name;
            this.IndexPath = Tunynet.Utilities.WebUtility.GetPhysicalFilePath(indexPath);
            this.DisplayOrder = displayOrder;
            searchEngine = SearcherFactory.GetSearchEngine(indexPath);
        }

        #region 搜索器属性

        /// <summary>
        /// 关联的搜索引擎实例
        /// </summary>
        public ISearchEngine SearchEngine
        {
            get
            {
                return searchEngine;
            }
        }

        /// <summary>
        /// 搜索器的唯一标识
        /// </summary>
        public string Code { get { return CODE; } }

        /// <summary>
        /// 显示顺序
        /// </summary>
        public int DisplayOrder { get; private set; }

        /// <summary>
        /// Lucene索引路径（完整物理路径，支持unc）
        /// </summary>
        public string IndexPath { get; private set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; private set; }

        #endregion 搜索器属性

        #region 索引内容维护

        /// <summary>
        /// 重建索引
        /// </summary>
        public void RebuildIndex()
        {
            //pageSize参数决定了每次批量取多少条数据进行索引。要注意的是，如果是分布式搜索，客户端会将这部分数据通过WCF传递给服务器端，而WCF默认的最大传输数据量是65535B，pageSize较大时这个设置显然是不够用的，WCF会报400错误；系统现在将最大传输量放宽了，但仍要注意一次不要传输过多，如遇异常，可适当调小pageSize的值
            int pageSize = 1000;
            int pageIndex = 1;
            long totalRecords = 0;
            bool isBeginning = true;
            bool isEndding = false;
            do
            {
                //分页获取列表
                PagingDataSet<ContentItem> contentItems = contentItemService.GetContentItemForAdmin(null, null, null, pageSize: pageSize, pageIndex: pageIndex);
                totalRecords = contentItems.TotalRecords;

                isEndding = (pageSize * pageIndex < totalRecords) ? false : true;

                //重建索引
                List<ContentItem> cmsList = contentItems.ToList<ContentItem>();

                IEnumerable<Document> docs = CmsIndexDocument.Convert(cmsList);

                searchEngine.RebuildIndex(docs, isBeginning, isEndding);

                isBeginning = false;
                pageIndex++;
            }
            while (!isEndding);
        }

        /// <summary>
        /// 添加索引
        /// </summary>
        /// <param name="documentIndex">待添加的资讯</param>
        public void Insert(Document documentIndex)
        {
            Insert(new Document[] { documentIndex });
        }

        /// <summary>
        /// 添加索引
        /// </summary>
        /// <param name="documentIndexs">待添加的资讯</param>
        public void Insert(IEnumerable<Document> documentIndexs)
        {
            searchEngine.Insert(documentIndexs);
        }

        /// <summary>
        /// 删除索引
        /// </summary>
        /// <param name="cmsContentItemId">待删除的资讯Id</param>
        public void Delete(long cmsContentItemId)
        {
            searchEngine.Delete(cmsContentItemId.ToString(), CmsIndexDocument.ContentItemId);
        }

        /// <summary>
        /// 删除索引
        /// </summary>
        /// <param name="cmsContentItemIds">待删除的资讯Id列表</param>
        public void Delete(IEnumerable<long> cmsContentItemIds)
        {
            foreach (var cmsContentItemId in cmsContentItemIds)
            {
                searchEngine.Delete(cmsContentItemId.ToString(), CmsIndexDocument.ContentItemId);
            }
        }

        /// <summary>
        /// 更新索引
        /// </summary>
        /// <param name="cms">待更新的资讯</param>
        public void Update(ContentItem cms)
        {
            Document doc = CmsIndexDocument.Convert(cms);
            searchEngine.Update(doc, cms.ContentItemId.ToString(), CmsIndexDocument.ContentItemId);
        }

        /// <summary>
        /// 更新索引
        /// </summary>
        /// <param name="contentItems">待更新的资讯集合</param>
        public void Update(IEnumerable<ContentItem> contentItems)
        {
            IEnumerable<Document> docs = CmsIndexDocument.Convert(contentItems);
            IEnumerable<string> cmsContentItemIds = contentItems.Select(n => n.ContentItemId.ToString());
            searchEngine.Update(docs, cmsContentItemIds, CmsIndexDocument.ContentItemId);
        }

        #endregion 索引内容维护

        #region 索引定时任务维护

        /// <summary>
        /// 定时任务操作
        /// </summary>
        public void SearchTask()
        {
            ////添加索引
            var insertValues = kvStore.Pop(KvKeys.Instance().CmsSearch(), 100);

            foreach (var value in insertValues)
            {
                long contentItemId = 0;

                value.TryParse<long>(out contentItemId);
                if (contentItemId > 0)
                {
                    var contentItem = contentItemService.Get(contentItemId);
                    if (contentItem != null)
                        Insert(CmsIndexDocument.Convert(contentItem));
                }
            }

            //删除索引
            var deleteValues = kvStore.Pop(KvKeys.Instance().CmsDeleteSearch(), 100);

            foreach (var value in deleteValues)
            {
                long contentItemId = 0;
                value.TryParse<long>(out contentItemId);

                if (contentItemId > 0)
                {
                    Delete(contentItemId);
                }
            }

            //更新索引
            var UpdateValues = kvStore.Pop(KvKeys.Instance().CmsUpdateSearch(), 100);

            foreach (var value in UpdateValues)
            {
                long contentItemId = 0;
                value.TryParse<long>(out contentItemId);

                var contentItem = contentItemService.Get(contentItemId);
                if (contentItem != null)
                    Update(contentItem);
            }

            Commit();
        }

        /// <summary>
        /// 提交索引变更
        /// </summary>
        public void Commit()
        {
            searchEngine.Commit();
        }

        #endregion 索引定时任务维护

        #region 搜索

        /// <summary>
        /// 资讯分页搜索
        /// </summary>
        /// <param name="cmsQuery">搜索条件</param>
        /// <returns>符合搜索条件的分页集合</returns>
        public PagingDataSet<ContentItem> Search(CmsFullTextQuery cmsQuery)
        {
            if (cmsQuery.ContentCategoryId == 0)
            {
                if (string.IsNullOrWhiteSpace(cmsQuery.Keyword))
                {
                    return new PagingDataSet<ContentItem>(new List<ContentItem>());
                }
            }

            var keywords = ClauseScrubber.SegmentForPhraseQuery(cmsQuery.Keyword);
            List<Document> searchResults = new List<Document>();

            foreach (var keyword in keywords)
            {
                cmsQuery.Keyword = keyword;

                LuceneSearchBuilder searchBuilder = BuildLuceneSearchBuilder(cmsQuery);

                //使用LuceneSearchBuilder构建Lucene需要Query、Filter、Sort
                Query query = null;
                Filter filter = null;
                Sort sort = null;
                searchBuilder.BuildQuery(out query, out filter, out sort);

                //调用SearchService.Search(),执行搜索
                searchResults.AddRange(searchEngine.Search(query, filter, sort));
            }

            //解析出搜索结果中的资讯ID
            List<long> cmsContentItemIds = new List<long>();

            foreach (Document doc in searchResults)
            {
                long cmsContentItemId = long.Parse(doc.Get(CmsIndexDocument.ContentItemId));
                if (!cmsContentItemIds.Contains(cmsContentItemId))
                    cmsContentItemIds.Add(cmsContentItemId);
            }

            var cmsIds = cmsContentItemIds.Skip(cmsQuery.PageSize * (cmsQuery.PageIndex - 1)).Take(cmsQuery.PageSize);

            //根据资讯ID列表批量查询资讯实例
            IEnumerable<ContentItem> contentItems = contentItemService.Gets(cmsIds);

            //组装分页对象
            return new PagingDataSet<ContentItem>(contentItems)
            {
                TotalRecords = cmsContentItemIds.LongCount(),
                PageSize = cmsQuery.PageSize,
                PageIndex = cmsQuery.PageIndex
            };
        }

        ///根据搜索查询条件构建Lucene查询条件
        private LuceneSearchBuilder BuildLuceneSearchBuilder(CmsFullTextQuery cmsQuery)
        {
            LuceneSearchBuilder searchBuilder = new LuceneSearchBuilder();
            //范围
            Dictionary<string, BoostLevel> fieldNameAndBoosts = new Dictionary<string, BoostLevel>();

            if (!string.IsNullOrEmpty(cmsQuery.Keyword))
            {
                switch (cmsQuery.Range)
                {
                    case Tunynet.Search.SearchRange.SUBJECT:
                        searchBuilder.WithPhrase(CmsIndexDocument.Subject, cmsQuery.Keyword, BoostLevel.Hight, false);
                        break;

                    case Tunynet.Search.SearchRange.BODY:
                        searchBuilder.WithPhrase(CmsIndexDocument.Body, cmsQuery.Keyword, BoostLevel.Hight, false);
                        break;

                    case Tunynet.Search.SearchRange.AUTHOR:
                        searchBuilder.WithPhrase(CmsIndexDocument.UserId, cmsQuery.Keyword, BoostLevel.Hight, false);
                        break;

                    default:
                        fieldNameAndBoosts.Add(CmsIndexDocument.Subject, BoostLevel.Hight);
                        fieldNameAndBoosts.Add(CmsIndexDocument.Summary, BoostLevel.Hight);
                        fieldNameAndBoosts.Add(CmsIndexDocument.Body, BoostLevel.Medium);
                        fieldNameAndBoosts.Add(CmsIndexDocument.Tags, BoostLevel.Medium);
                        fieldNameAndBoosts.Add(CmsIndexDocument.UserId, BoostLevel.Low);
                        searchBuilder.WithPhrases(fieldNameAndBoosts, cmsQuery.Keyword, Occur.SHOULD, false);
                        break;
                }
            }

            //某个栏目
            if (cmsQuery.ContentCategoryId > 0)
            {
                searchBuilder.WithField(CmsIndexDocument.ContentCategoryId, cmsQuery.ContentCategoryId.ToString(), true, BoostLevel.Hight, true);
            }

            if (cmsQuery.MinDate.HasValue && cmsQuery.MaxDate.HasValue)
            {
                searchBuilder.WithinRange(CmsIndexDocument.DatePublished, DateTools.DateToString(cmsQuery.MinDate.Value, DateTools.Resolution.MINUTE), DateTools.DateToString(cmsQuery.MaxDate.Value, DateTools.Resolution.MINUTE));
            }

            if (!cmsQuery.IsDefaultOrder)
            {
                //资讯排序
                searchBuilder.SortByString(CmsIndexDocument.DatePublished, true);
            }

            if (cmsQuery.PubliclyAuditStatus.HasValue)
            {
                switch (cmsQuery.PubliclyAuditStatus.Value)
                {
                    case Tunynet.Common.PubliclyAuditStatus.Again:
                        searchBuilder.WithinRange(CmsIndexDocument.ApprovalStatus, ((int)Tunynet.Common.PubliclyAuditStatus.Pending).ToString(), ((int)Tunynet.Common.PubliclyAuditStatus.Success).ToString());
                        break;

                    case Tunynet.Common.PubliclyAuditStatus.Fail:
                    case Tunynet.Common.PubliclyAuditStatus.Pending_GreaterThanOrEqual:
                    case Tunynet.Common.PubliclyAuditStatus.Pending:
                    case Tunynet.Common.PubliclyAuditStatus.Again_GreaterThanOrEqual:
                        searchBuilder.WithinRange(CmsIndexDocument.ApprovalStatus, ((int)Tunynet.Common.PubliclyAuditStatus.Fail).ToString(), ((int)Tunynet.Common.PubliclyAuditStatus.Success).ToString());
                        break;

                    case Tunynet.Common.PubliclyAuditStatus.Success:
                    default:
                        searchBuilder.WithinRange(CmsIndexDocument.ApprovalStatus, ((int)Tunynet.Common.PubliclyAuditStatus.Success).ToString(), ((int)Tunynet.Common.PubliclyAuditStatus.Success).ToString());
                        break;
                }
            }

            return searchBuilder;
        }

        #endregion 搜索
    }
}