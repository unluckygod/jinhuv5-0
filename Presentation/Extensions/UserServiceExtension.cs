﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet;
using Tunynet.Common;

namespace Tunynet.Common
{
    /// <summary>
    /// 扩展用户业务逻辑
    /// </summary>
    public static class UserServiceExtension
    {
        /// <summary>
        /// 获取用户的回答数
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="userId">userId</param>
        /// <param name="isIgnoreAuditStatus">是否忽略审核</param>
        /// <returns></returns>
        public static int GetUserAnswerCount(this IUserService userService, long userId, bool isIgnoreAuditStatus = false)
        {
            IKvStore kvStore = DIContainer.Resolve<IKvStore>();
            //待审核,需在审核,通过个数
            int pendingCount, againCount, successCount, returnCount = 0;
            if (isIgnoreAuditStatus)
                kvStore.TryGet<int>(KvKeys.Instance().UserAskAnswerCount(userId, null), out returnCount);
            else
            {
                kvStore.TryGet<int>(KvKeys.Instance().UserAskAnswerCount(userId, AuditStatus.Pending), out pendingCount);
                kvStore.TryGet<int>(KvKeys.Instance().UserAskAnswerCount(userId, AuditStatus.Again), out againCount);
                kvStore.TryGet<int>(KvKeys.Instance().UserAskAnswerCount(userId, AuditStatus.Success), out successCount);
                userService.CalculateCount(pendingCount, againCount, successCount, ref returnCount);
            }
            return returnCount;
        }

        /// <summary>
        /// 获取用户的发布的问题数
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="userId"></param>
        /// <param name="isIgnoreAuditStatus">是否忽略审核</param>
        /// <returns></returns>
        public static int GetUserQuestionCount(this IUserService userService, long userId, bool isIgnoreAuditStatus = false)
        {
            IKvStore kvStore = DIContainer.Resolve<IKvStore>();
            //待审核,需在审核,通过个数
            int pendingCount, againCount, successCount, returnCount = 0;
            if (isIgnoreAuditStatus)
                kvStore.TryGet<int>(KvKeys.Instance().UserAskQuestionCount(userId, null), out returnCount);
            else
            {
                kvStore.TryGet<int>(KvKeys.Instance().UserAskQuestionCount(userId, AuditStatus.Pending), out pendingCount);
                kvStore.TryGet<int>(KvKeys.Instance().UserAskQuestionCount(userId, AuditStatus.Again), out againCount);
                kvStore.TryGet<int>(KvKeys.Instance().UserAskQuestionCount(userId, AuditStatus.Success), out successCount);
                userService.CalculateCount(pendingCount, againCount, successCount, ref returnCount);
            }
            return returnCount;
        }

        /// <summary>
        /// 获取用户的活动计数
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="userId">userId</param>
        /// <param name="isIgnoreAuditStatus">是否忽略审核</param>
        /// <returns></returns>
        public static int GetUserEventCount(this IUserService userService, long userId, bool isIgnoreAuditStatus = false)
        {
            IKvStore kvStore = DIContainer.Resolve<IKvStore>();
            //待审核,需在审核,通过个数
            int pendingCount, againCount, successCount, returnCount = 0;
            if (isIgnoreAuditStatus)
                kvStore.TryGet<int>(KvKeys.Instance().UserEventCount(userId, null), out returnCount);
            else
            {
                kvStore.TryGet<int>(KvKeys.Instance().UserEventCount(userId, AuditStatus.Pending), out pendingCount);
                kvStore.TryGet<int>(KvKeys.Instance().UserEventCount(userId, AuditStatus.Again), out againCount);
                kvStore.TryGet<int>(KvKeys.Instance().UserEventCount(userId, AuditStatus.Success), out successCount);
                userService.CalculateCount(pendingCount, againCount, successCount, ref returnCount);
            }
            return returnCount;
        }
    }
}