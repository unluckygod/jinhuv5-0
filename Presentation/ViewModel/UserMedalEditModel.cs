﻿using System.ComponentModel.DataAnnotations;

namespace Tunynet.Common
{
    public class UserMedalEditModel
    {
        /// <summary>
        /// 新建实体时使用
        /// </summary>
        public static UserMedalEditModel New()
        {
            UserMedalEditModel model = new UserMedalEditModel()
            {
                MedalName = string.Empty,
                Description = string.Empty,
                Conditions = string.Empty,
                ConditionValues = string.Empty,
                AwardType = AwardType.AwardByOther,
                AwardStatus = AwardStatus.AllowAward
            };
            return model;
        }

        /// <summary>
        /// 勋章主键标识
        /// </summary>
        public long MedalId { get; set; }

        /// <summary>
        ///勋章标题图Id
        /// </summary>
        [RegularExpression(pattern: "^[1-9][0-9]{1,}$", ErrorMessage = "请上传勋章标识图片")]
        [Display(Name = "勋章标识图片")]
        public long ImageAttachmentId { get; set; }

        /// <summary>
        /// 勋章名称
        /// </summary>
        [Required(ErrorMessage = "请输入勋章名")]
        [StringLength(64, ErrorMessage = "最多可以输入64个字")]
        [Display(Name = "勋章名称")]
        public string MedalName { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        [Required(ErrorMessage = "请输入勋章说明")]
        [StringLength(512, ErrorMessage = "最多可以输入512个字")]
        [Display(Name = "说明")]
        public string Description { get; set; }

        /// <summary>
        /// 互斥组Id
        /// </summary>
        [Display(Name = "互斥组")]
        public long GroupId { get; set; }

        /// <summary>
        /// 授予状态（可以授予、停止授予）
        /// </summary>
        [Display(Name = "授予状态")]
        [Required(ErrorMessage = "请选择授予状态")]
        public AwardStatus AwardStatus { get; set; }

        /// <summary>
        /// 授予方式（自主申请、人工授予）
        /// </summary>
        [Display(Name = "授予方式")]
        [Required(ErrorMessage = "请选择授予方式")]
        public AwardType AwardType { get; set; }

        /// <summary>
        /// 申请条件Id
        /// </summary>
        [Display(Name = "申请条件")]
        public string Conditions { get; set; }

        /// <summary>
        /// 申请条件最小值
        /// </summary>
        public string ConditionValues { get; set; }
    }
}