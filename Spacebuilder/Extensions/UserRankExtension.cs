﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 用户等级扩展
    /// </summary>
    public static class UserRankExtension
    {
        /// <summary>
        /// 用户等级图标获取
        /// </summary>
        /// <param name="rank"></param>
        /// <returns></returns>
        public static string rankimage(int rank)
        {
            string rankimages = string.Empty;
            var one = rank / 9;
            var two = (rank - 9 * one) / 3;
            var thr = rank - 9 * one - 3 * two;
            for (int i = 0; i < one; i++)
            {
                rankimages = rankimages + @"<li><img alt='...' src='/img/star1.png'></li>";
            }
            for (int i = 0; i < two; i++)
            {
                rankimages = rankimages + @"<li><img alt='...' src='/img/star2.png'></li>";
            }
            for (int i = 0; i < thr; i++)
            {
                rankimages = rankimages + @"<li><img alt='...' src='/img/star3.png'></li>";
            }
            return rankimages;
        }
    }
}