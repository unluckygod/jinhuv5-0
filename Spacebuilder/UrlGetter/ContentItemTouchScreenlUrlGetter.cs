﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.CMS;
using Tunynet.Common;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 资讯触屏版url获取
    /// </summary>
    public class ContentItemTouchScreenlUrlGetter : ITouchScreenUrlGetter
    {
        /// <summary>
        /// 租户类型Id
        /// </summary>
        public string TenantTypeId
        {
            get { return TenantTypeIds.Instance().ContentItem(); }
        }

        /// <summary>
        /// 获取资讯详情的触屏版url
        /// </summary>
        /// <param name="objectId">对象ID</param>
        /// <returns></returns>
        public string GetTouchScreenDetailUrl(long objectId)
        {
            ContentItemRepository contentItemRepository = new ContentItemRepository();
            var contentItem = contentItemRepository.Get(objectId);
            var touchScreenDetailUrl = "html/newsDetail.html?ContentItemId=";
            if (contentItem != null)
            {
                if (contentItem.ContentModel.ModelKey == ContentModelKeys.Instance().Image())
                    touchScreenDetailUrl = "html/imgsDetail.html?ContentItemId=";
                else if (contentItem.ContentModel.ModelKey == ContentModelKeys.Instance().Video())
                    touchScreenDetailUrl = "html/videoDetail.html?ContentItemId=";
            }

            var ipUrl = Utility.GetTouchScreenUrl();
            ipUrl = $"{ipUrl}/{touchScreenDetailUrl}{objectId}";
            return ipUrl;
        }
    }
}