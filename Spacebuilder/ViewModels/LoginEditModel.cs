﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------
using System.ComponentModel.DataAnnotations;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 登录模板
    /// </summary>
    public class LoginEditModel
    {
        /// <summary>
        /// 用户名、手机号
        /// </summary>
        [Required(ErrorMessage = "请输登录帐号")]
        //[Remote("CheckUser", "Account", ErrorMessage = "请输入有效手机号或邮箱")]
        public string Name { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "请输入密码")]
        public string PassWord { get; set; }

        /// <summary>
        /// 是否记得密码
        /// </summary>
        [Display(Name = "下次自动登录")]
        public bool RememberPassword { get; set; }

        /// <summary>
        /// 跳转地址
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}