﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using PetaPoco;
using System;
using Tunynet.Caching;
using Tunynet.Common;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 用户资料
    /// </summary>
    [TableName("spb_UserProfiles")]
    [PrimaryKey("UserId", autoIncrement = false)]
    [CacheSetting(true, ExpirationPolicy = EntityCacheExpirationPolicies.Usual)]
    [Serializable]
    public class UserProfile : SerializablePropertiesBase, IEntity
    {
        /// <summary>
        /// 新建实体时使用
        /// </summary>
        public static UserProfile New(long userId)
        {
            UserProfile userProfile = new UserProfile()
            {
                Birthday = DateTime.Now,
                LunarBirthday = DateTime.Now,
                BirthdayType = BirthdayType.Birthday,
                QQ = string.Empty,
                CardID = string.Empty,
                Introduction = string.Empty,
                CardType = CertificateType.Residentcard,
                NowAreaCode = string.Empty,
                Integrity = 0,
                Gender = GenderType.NotSet,
                UserId = userId
            };
            return userProfile;
        }

        #region 需持久化属性

        /// <summary>
        ///UserId
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        ///性别1=男,2=女,0=未设置
        /// </summary>
        public GenderType Gender { get; set; }

        /// <summary>
        ///生日类型1=公历,2=阴历
        /// </summary>
        public BirthdayType BirthdayType { get; set; }

        /// <summary>
        ///公历生日
        /// </summary>
        public DateTime Birthday { get; set; }

        /// <summary>
        ///阴历生日
        /// </summary>
        public DateTime LunarBirthday { get; set; }

        /// <summary>
        ///所在地
        /// </summary>
        public string NowAreaCode { get; set; }

        /// <summary>
        ///QQ
        /// </summary>
        public string QQ { get; set; }

        /// <summary>
        ///证件类型
        /// </summary>
        public CertificateType CardType { get; set; }

        /// <summary>
        ///证件号码
        /// </summary>
        public string CardID { get; set; }

        /// <summary>
        ///自我介绍
        /// </summary>
        public string Introduction { get; set; }

        /// <summary>
        /// 资料完整度（0至100）
        /// </summary>
        public int Integrity { get; set; }

        #endregion 需持久化属性

        #region 扩展属性

        /// <summary>
        /// 是都存在IM
        /// </summary>
        [Ignore]
        public bool HasIM
        {
            get
            {
                return !string.IsNullOrEmpty(QQ);
            }
        }

        /// <summary>
        /// 检查所在地是否存在
        /// </summary>
        [Ignore]
        public bool HasNowAreaCode { get { return !string.IsNullOrEmpty(NowAreaCode); } }

        /// <summary>
        /// 自我介绍是否存在
        /// </summary>
        [Ignore]
        public bool HasIntroduction { get { return !string.IsNullOrEmpty(Introduction); } }

        /// <summary>
        /// 用户的第三人称
        /// </summary>
        /// <returns></returns>
        [Ignore]
        public string ThirdPerson
        {
            get
            {
                if (UserContext.CurrentUser != null && UserContext.CurrentUser.UserId == this.UserId)
                {
                    return "我";
                }

                string resourceKey = "Common_";
                switch (this.Gender)
                {
                    case GenderType.FeMale:
                        resourceKey += "She";
                        break;

                    case GenderType.Male:
                        resourceKey += "He";
                        break;

                    default:
                        resourceKey += "Ta";
                        break;
                }
                return Tunynet.ResourceAccessor.GetString(resourceKey);
            }
        }

        /// <summary>
        /// 是否使用自定义皮肤
        /// </summary>
        [Ignore]
        public bool IsUseCustomStyle
        {
            get
            {
                return GetExtendedProperty<bool>("IsUseCustomStyle");
            }
            set
            {
                SetExtendedProperty("IsUseCustomStyle", value);
            }
        }

        /// <summary>
        /// 皮肤标识
        /// </summary>
        [Ignore]
        public string ThemeAppearance
        {
            get
            {
                return GetExtendedProperty<string>("ThemeAppearance");
            }
            set
            {
                SetExtendedProperty("ThemeAppearance", value);
            }
        }

        #endregion 扩展属性

        #region IEntity 成员

        object IEntity.EntityId { get { return this.UserId; } }

        bool IEntity.IsDeletedInDatabase { get; set; }

        #endregion IEntity 成员
    }
}