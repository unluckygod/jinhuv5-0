﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.Common;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 扩展用户资料
    /// </summary>
    public static class UserExtension
    {
        /// <summary>
        /// 用户资料
        /// </summary>
        public static UserProfile Profile(this IUser user)
        {
            return DIContainer.Resolve<UserProfileService>().Get(user.UserId);
        }
    }
}